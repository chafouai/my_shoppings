Small shopping list app (AngularJS and Ionic 1) that implements the following user journeys.

- UJ1: The user wants to go shopping and adds some items to her shopping list.
- UJ2: Before she leaves home she’d love to know how much money she might spend. Therefore, she expects to see how much each item might cost.
- UJ3: To be more effective, the user wants to sort the items on the shopping list before she goes shopping in categories.


