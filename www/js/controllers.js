//home controller
app.controller('HomeCtrl',function($scope,$state,ShoppingFactory){
	$scope.shoppings = ShoppingFactory.getShoppings();
    


    $scope.addShopping = function(shopping){
      
      $scope.newShopping = {
        id: $scope.shoppings.length,
        title: shopping,
        products: []
      }

      $scope.shoppings.push($scope.newShopping)
      $state.go('shopping',{id: $scope.newShopping.id})

      ShoppingFactory.saveShopping();
    }

    $scope.deleteShopping = function(id){
      alert(id)
      for(var i = 0; i <$scope.shoppings.length; i++){
        if($scope.shoppings[i].id === id){
          $scope.shoppings.splice(i, 1);
          ShoppingFactory.saveShopping();
          return;
        }
      }
    }
    $scope.editShopping = function(id){
      //alert(id);
      $state.go('shopping',{id: id})
    }

    $scope.getPrice = function(shopping){
        var price = 0;
        if(shopping.products!=null){
          angular.forEach(shopping.products, function(product,key){
            price += product.price;
          })
        }
        return price;
    }
})




//shopping controller
app.controller('ShoppingCtrl',function($scope,$stateParams,$state,ShoppingFactory){
	$scope.shopping = ShoppingFactory.getShopping($stateParams.id)
	//$scope.shopping = $stateParams.shopping  
	$scope.addProduct = function(id,query){
	  $state.go('product',{id: id,query: query})
	  ShoppingFactory.saveShopping();
	}
	$scope.deleteProduct = function(name){
	  alert(name)
	  let products = $scope.shopping.products;

	  for(var i = 0; i <products.length; i++){
	    if(products[i].name === name){
	      products.splice(i, 1);
	      ShoppingFactory.saveShopping();
	      return;
	    }
	  }
	}
	$scope.getPrice = function(shopping){
        var price = 0;
        if(shopping.products!=null){
          angular.forEach(shopping.products, function(product,key){
            price += product.price;
          })
        }
        return price;
    }
})




//product controller
app.controller('ProductCtrl',function($scope,$stateParams,$http,$ionicLoading,ShoppingFactory){
	$scope.shopping = ShoppingFactory.getShopping($stateParams.id)
	$scope.query = $stateParams.query

	$ionicLoading.show({
	    template: 'Loading...',
	    duration: 3000
	  }).then(function(){
	     console.log("The loading indicator is now displayed");
	  });
	url = "https://www.bringmeister.de/api/products?limit=10&offset=0&q="+$scope.query  
	$http.get(url).success(function(response){
	$ionicLoading.hide();
	$scope.products = response.products
	})
	$scope.productSelect = function(shopping,name,category,image,price){
	  var product = {name: name, category: category, image: image, price: price}
	  console.log(product)
	  shopping.products.push(product);
	  ShoppingFactory.saveShopping();
	  alert(name+" added to your shopping list : \”"+shopping.title+"\”");
	}
})